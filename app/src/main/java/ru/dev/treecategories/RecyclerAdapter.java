package ru.dev.treecategories;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * RecyclerAdapter
 */

class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    final static String TITLE = "title";
    final static String SUBS = "subs";

    private JSONArray dataSet;
    private ItemClickListener onItemClickListener;

    RecyclerAdapter(JSONArray dataSet) {

        this.dataSet = dataSet;
    }

    /**
     * Change data set
     * @param newDataSet new json array
     */
    void setDataSet(JSONArray newDataSet) {

        this.dataSet = newDataSet;
        notifyDataSetChanged();
    }

    interface ItemClickListener {
        void onClick(View view, int position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try {
            if (getItemCount() > 0) {
                JSONObject jsonObject = (JSONObject) dataSet.get(position);

                // title for item
                if (jsonObject.has(TITLE)) {
                    holder.tvItemTitle.setText(jsonObject.getString(TITLE));
                }

                // image for item
                if (jsonObject.has(SUBS)) {
                    holder.tvItemTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_folder, 0, 0, 0);
                } else {
                    holder.tvItemTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_doc, 0, 0, 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return dataSet == null ? 0 : dataSet.length();
    }

    void setOnItemClickListener(ItemClickListener onItemClickListener) {

        this.onItemClickListener = onItemClickListener;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvItemTitle;

        ViewHolder(View view) {

            super(view);
            tvItemTitle = (TextView) view.findViewById(R.id.tvItemTitle);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            if (onItemClickListener != null) {
                onItemClickListener.onClick(view, getAdapterPosition());
            }
        }
    }
}
