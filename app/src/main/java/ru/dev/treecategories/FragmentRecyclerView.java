package ru.dev.treecategories;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * FragmentRecyclerView
 */
public class FragmentRecyclerView extends Fragment {

    private final String KEY_TITLE_STATE = "key_title_state";
    private final String KEY_IS_ROOT_STATE = "key_is_root_state";
    private final String KEY_JSON_ARRAY_DATA_STATE = "key_json_array_data_state";
    private final String KEY_RECYCLER_VIEW_STATE = "key_recycler_view_state";

    String title;

    private RecyclerView recyclerView;
    private GridLayoutManager gridLayoutManager;

    private String jsonString;
    private JSONArray jsonArrayData;
    private RecyclerAdapter.ItemClickListener itemClickListener;
    private RecyclerAdapter recyclerAdapter;

    public boolean isRoot;

    Parcelable recyclerViewState;

    public void setJsonString(String jsonString) {

        this.jsonString = jsonString;
    }

    /**
     * Initialization json array
     */
    private void initJsonArrayData() {

        try {
            jsonArrayData = new JSONArray(jsonString);
        } catch (Exception e) {
            jsonArrayData = new JSONArray();
            e.printStackTrace();
        }
    }

    public void changeJsonData(String jsonString) {

        this.jsonString = jsonString;
        initJsonArrayData();
        if (recyclerAdapter == null) {
            recyclerAdapter = new RecyclerAdapter(this.jsonArrayData);
            recyclerAdapter.setOnItemClickListener(itemClickListener);
        } else {
            recyclerAdapter.setDataSet(this.jsonArrayData);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            // restore title
            title = savedInstanceState.getString(KEY_TITLE_STATE);

            // restore isRoot
            isRoot = savedInstanceState.getBoolean(KEY_IS_ROOT_STATE);

            // restore json data
            jsonString = savedInstanceState.getString(KEY_JSON_ARRAY_DATA_STATE);

            // restore recycler view state
            recyclerViewState = savedInstanceState.getParcelable(KEY_RECYCLER_VIEW_STATE);
        }
        initJsonArrayData();

        itemClickListener = new RecyclerAdapter.ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                try {
                    JSONObject jsonObject = (JSONObject) jsonArrayData.get(position);
                    if (jsonObject != null && jsonObject.has(RecyclerAdapter.SUBS)) {
                        JSONArray jsonArraySubs = null;
                        try {
                            jsonArraySubs = jsonObject.getJSONArray(RecyclerAdapter.SUBS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (jsonArraySubs != null) {
                            ((ActivityMain) getActivity()).openCategory(
                                    jsonObject.getString(RecyclerAdapter.TITLE),
                                    jsonArraySubs.toString(),
                                    false
                            );
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final ViewGroup parentNull = null;
        View view = inflater.inflate(R.layout.fragment, parentNull);

        ((ActivityMain) getActivity()).setTitle(title, isRoot);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        // use GridLayoutManager
        setRecyclerViewGridManager();

        // adapter
        recyclerAdapter = new RecyclerAdapter(jsonArrayData);
        recyclerAdapter.setOnItemClickListener(itemClickListener);
        recyclerView.setAdapter(recyclerAdapter);

        return view;
    }

    private void setRecyclerViewGridManager() {

        int scrollPosition = 0;

        int spanCount = 1;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            spanCount = 2;
        }

        // If a layout manager has already been set, get current scroll position.
        if (recyclerView.getLayoutManager() != null) {
            scrollPosition = ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        } else if (recyclerViewState != null) {
            if (gridLayoutManager == null) {
                gridLayoutManager = new GridLayoutManager(getActivity(), spanCount);
            }
            gridLayoutManager.onRestoreInstanceState(recyclerViewState);
            recyclerView.setLayoutManager(gridLayoutManager);
        } else {
            gridLayoutManager = new GridLayoutManager(getActivity(), spanCount);
            recyclerView.setLayoutManager(gridLayoutManager);
        }

        recyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        // save state isRoot
        outState.putBoolean(KEY_IS_ROOT_STATE, isRoot);

        // save state title
        outState.putString(KEY_TITLE_STATE, title);

        // save state json string
        outState.putString(KEY_JSON_ARRAY_DATA_STATE, jsonArrayData.toString());

        // save state recycler view
        if (recyclerViewState != null) {
            recyclerViewState = recyclerView.getLayoutManager().onSaveInstanceState();
            outState.putParcelable(KEY_RECYCLER_VIEW_STATE, recyclerViewState);
        }

        super.onSaveInstanceState(outState);
    }
}
