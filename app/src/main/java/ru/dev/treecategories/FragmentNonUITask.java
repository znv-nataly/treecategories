package ru.dev.treecategories;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Fragment non UI
 */

public class FragmentNonUITask extends Fragment {

    AsyncTaskGetData task;
    Activity activity;

    public void beginTask(String url) {

        task = new AsyncTaskGetData(activity, url);
        task.execute();
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);
        this.activity = (Activity) context;

        if (task != null) {
            task.onAttach(this.activity);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return null; // non UI
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true); // fragment won't be deleted
    }

    @Override
    public void onDetach() {

        super.onDetach();

        if (task != null) {
            task.onDetach();
        }
    }
}
