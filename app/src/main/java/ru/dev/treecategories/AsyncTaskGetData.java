package ru.dev.treecategories;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * AsyncTaskGetData
 */

class AsyncTaskGetData extends AsyncTask<Void, Void, String> {

    private String url;
    private Activity activity;

    AsyncTaskGetData(Activity activity, String url) {

        super();
        this.url = url;
        onAttach(activity);
    }

    void onAttach(Activity activity) {
        this.activity = activity;
    }

    void onDetach() {
        this.activity = null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (activity != null) {
            ((ActivityMain) activity).setVisibleProgressBar(View.VISIBLE);
        }
    }

    @Override
    protected String doInBackground(Void... voids) {

        try {
            return getJsonString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String jsonString) {

        super.onPostExecute(jsonString);
        if (activity != null) {
            ((ActivityMain) activity).openCategory(activity.getResources().getString(R.string.app_name), jsonString, true);
            ((ActivityMain) activity).setVisibleProgressBar(View.GONE);
        }
    }

    /**
     * Get JSONArray by url
     * @return JSONArray
     * @throws Exception
     */
    private String getJsonString() throws Exception {

        URL url = new URL(this.url);

        // чтение данных
        Reader reader = new InputStreamReader(url.openStream());

        return readStream(reader);
    }

    /**
     * Read data from stream
     * @param reader stream data
     * @return String
     * @throws Exception
     */
    private String readStream(Reader reader) throws Exception {

        BufferedReader bufferedReader = new BufferedReader(reader);

        String str = "";
        String line;
        while((line = bufferedReader.readLine()) != null) {
            str += line + "\n";
        }
        bufferedReader.close();
        reader.close();

        return str;
    }
}
