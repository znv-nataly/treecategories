package ru.dev.treecategories;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ActivityMain extends FragmentActivity {

    final String URL_STRING = "https://dl.dropboxusercontent.com/u/50542096/categories-list.json";
    private TextView tvTitle;
    private ProgressBar progressBar;
    private FragmentNonUITask fragmentNonUITask;
    private FragmentRecyclerView fragmentRecyclerViewRoot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            // header
            tvTitle = (TextView) findViewById(R.id.tvTitle);

            // reload data
            ImageView ivRefresh = (ImageView) findViewById(R.id.ivRefresh);
            ivRefresh.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadData();
                }
            });

            // view process load data
            progressBar = (ProgressBar) findViewById(R.id.progressBar);

            String tagFragmentNonUI = "fragmentNonUITask";
            if (savedInstanceState == null) {
                setVisibleProgressBar(View.VISIBLE);
                fragmentNonUITask = new FragmentNonUITask();
                getSupportFragmentManager().beginTransaction().add(fragmentNonUITask, tagFragmentNonUI).commit();
                loadData();
            } else {
                fragmentNonUITask = (FragmentNonUITask) getSupportFragmentManager().findFragmentByTag(tagFragmentNonUI);
                fragmentRecyclerViewRoot = getFragmentRootFromBackStack();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get root fragment from BackStack
     * @return FragmentRecyclerView
     */
    private FragmentRecyclerView getFragmentRootFromBackStack() {

        List<Fragment> list = getSupportFragmentManager().getFragments();
        try {
            // 0 - FragmentNonUITask
            return  (FragmentRecyclerView) list.get(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            finish();
        }
    }

    /**
     * Open category. View new FragmentRecyclerView
     * @param title title category
     * @param jsonString structure of category
     */
    public void openCategory(String title, String jsonString, boolean isRoot) {

        setTitle(title, isRoot);

        if (isRoot && fragmentRecyclerViewRoot != null) {
            // just refresh data
            fragmentRecyclerViewRoot.changeJsonData(jsonString);
        } else {

            FragmentRecyclerView fragmentNew = new FragmentRecyclerView();
            fragmentNew.setJsonString(jsonString);
            fragmentNew.title = title;
            fragmentNew.isRoot = isRoot;

            int count = getFragmentManager().getBackStackEntryCount();
            String tagName = "fragment_" + count;
            getSupportFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.container, fragmentNew)
                    .addToBackStack(tagName)
                    .commit();

            if (isRoot) {
                fragmentRecyclerViewRoot = fragmentNew;
            }
        }

        if (jsonString == null || jsonString.isEmpty()) {
            Toast.makeText(this, R.string.error_get_data, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Set header activity
     * @param title string header
     * @param isRoot bool
     */
    public void setTitle(String title, boolean isRoot) {

        tvTitle.setText(title);
        if (!isRoot) {
            tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ic_arrow, 0, 0, 0);
            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        } else {
            tvTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            tvTitle.setOnClickListener(null);
        }
    }

    /**
     * Load data with using AsyncTask
     */
    public void loadData() {

        setTitle(getResources().getString(R.string.app_name), true);

        int count = getSupportFragmentManager().getBackStackEntryCount();
        // pop fragments besides root
        while(count > 1) {
            getSupportFragmentManager().popBackStackImmediate();
            count = getSupportFragmentManager().getBackStackEntryCount();
        }

        fragmentRecyclerViewRoot = getFragmentRootFromBackStack();

        fragmentNonUITask.beginTask(URL_STRING);
    }

    public void setVisibleProgressBar(int visible) {

        progressBar.setVisibility(visible);
    }
}
